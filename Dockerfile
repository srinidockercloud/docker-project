FROM ubuntu:16.04

RUN mkdir app

COPY . /app

WORKDIR /app

RUN apt-get update \
    && apt-get install -y nginx

RUN cp /app/nginx/nginx.conf /etc/nginx/nginx.conf \
    && cp /app/index.html /var/www/html/index.html

CMD service nginx start
